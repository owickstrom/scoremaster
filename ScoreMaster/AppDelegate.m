//
//  AppDelegate.m
//  ScoreMaster
//
//  Created by Oskar (Privat) on 2013-07-08.
//  Copyright (c) 2013 Oskar Wickström. All rights reserved.
//

#import "AppDelegate.h"
#import "ScoreSpecification.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    ScoreSpecification *defaultScoreSpecification = [[ScoreSpecification alloc] initWithKeySignature:@"C" AndMode:@"Ionian" AndDifficulty:@1];
    self.scoreSpecification = defaultScoreSpecification;
    
    [self.scoreSpecification addObserver:self forKeyPath:@"keySignature" options:NSKeyValueObservingOptionNew context:NULL];
    [self.scoreSpecification addObserver:self forKeyPath:@"mode" options:NSKeyValueObservingOptionNew context:NULL];
    [self.scoreSpecification addObserver:self forKeyPath:@"difficulty" options:NSKeyValueObservingOptionNew context:NULL];
    
    self.keySignatures = [NSArray arrayWithObjects:@"C",@"D",@"E",@"F",@"G",@"A",@"B",nil];
    self.modes = [NSArray arrayWithObjects:@"Ionian",@"Dorian",@"Phrygian",@"Lydian",@"Mixolydian",@"Aeolian",@"Locrian",nil];
    self.difficulties = [NSArray arrayWithObjects:@1,@2,@3,nil];
    
    [self refreshScoreImage];
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([object isEqualTo:self.scoreSpecification])
    {
        [self refreshScoreImage];
    }
}

- (NSInteger)showAlert:(NSString*)title withInfo:(NSString*)info
{
    NSString *cancelButton = NSLocalizedString(@"OK", nil);
    
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:title];
    [alert setInformativeText:info];
    [alert addButtonWithTitle:cancelButton];
    
    return [alert runModal];
}

-(void) fadeOutScoreImage
{
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:0.05f];
    [self.scoreImageView.animator setAlphaValue:0.0];
    [NSAnimationContext endGrouping];
}
-(void) fadeInScoreImage
{
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:0.05f];
    [self.scoreImageView.animator setAlphaValue:1.0];
    [NSAnimationContext endGrouping];
}

-(void) setRefreshStarted
{
    self.isRefreshing = @1;
    
    [self.progressIndicator setIndeterminate:NO];
    [self.progressIndicator startAnimation:self];
    
    [self performSelectorOnMainThread:@selector(fadeOutScoreImage) withObject:nil waitUntilDone:YES];
}
-(void) setRefreshFinished
{
    self.isRefreshing = @0;
    
    [self.progressIndicator stopAnimation:self];
    
    self.isRefreshing = @0;
    [self performSelectorOnMainThread:@selector(fadeInScoreImage) withObject:nil waitUntilDone:YES];
    self.progress = @0;
}

-(void) refreshScoreImage
{
    [self setRefreshStarted];
    
    NSString *imageUrlString = [NSString stringWithFormat:
                                @"http://random-score.herokuapp.com/random-score.png?key=%@&mode=%@&difficulty=%@#%ld",
                                self.scoreSpecification.keySignature,
                                [self.scoreSpecification.mode uppercaseString],
                                [self.scoreSpecification difficultyAsString],
                                (long)[[NSDate date] timeIntervalSince1970]];
    NSLog(@"Fetching new image at %@", imageUrlString);
    
    NSURL *myURL = [NSURL URLWithString:imageUrlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:myURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseData = [[NSMutableData alloc] init];
    
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    long status = [httpResponse statusCode];
    
    if(status == 200L) {
        self.expectedResponseSize = [NSNumber numberWithLongLong:[response expectedContentLength]];
    } else {
        [self showAlert:@"Failed to refresh score!" withInfo:@"There was a problem with the remote score rendering service."];
        [connection cancel];
        [self setRefreshFinished];
    }
    self.progress = @0;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    self.progress = [NSNumber numberWithLongLong: ([self.progress longLongValue] + [data length])];
    NSLog(@"%@", self.progress);
    [self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Unable to fetch data: %@", error.description);
    
    
    [self showAlert:@"Failed to refresh score!" withInfo:[error localizedDescription]];
    [self setRefreshFinished];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Received %ld bytes of data",(unsigned long)[self.responseData length]);
    self.scoreImage = [[NSImage alloc] initWithData:self.responseData];
    
    [self setRefreshFinished];
}

- (IBAction)buttonClicked:(id)sender {
    [self refreshScoreImage];
}

@end
