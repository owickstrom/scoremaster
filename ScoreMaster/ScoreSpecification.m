//
//  ScoreSpecification.m
//  ScoreMaster
//
//  Created by Oskar (Privat) on 2013-07-08.
//  Copyright (c) 2013 Oskar Wickström. All rights reserved.
//

#import "ScoreSpecification.h"

@implementation ScoreSpecification

- (id)initWithKeySignature:(NSString*)keySignature AndMode:(NSString*)mode AndDifficulty:(NSNumber*)difficulty
{
    if(self = [super init])
    {
        self.keySignature = keySignature;
        self.mode = mode;
        self.difficulty = difficulty;
    }
    
    return self;
}


-(NSString*) difficultyAsString
{
    NSArray *names = [NSArray arrayWithObjects:@"EASY",@"MEDIUM",@"HARD", nil];
    NSInteger i = [self.difficulty intValue] - 1;
     
    if(i >= 0 && i < [names count])
    {
        return [names objectAtIndex:i];
    }
    else
    {
        return nil;
    }
}

@end
