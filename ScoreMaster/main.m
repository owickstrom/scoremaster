//
//  main.m
//  ScoreMaster
//
//  Created by Oskar (Privat) on 2013-07-08.
//  Copyright (c) 2013 Oskar Wickström. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
