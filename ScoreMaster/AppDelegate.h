//
//  AppDelegate.h
//  ScoreMaster
//
//  Created by Oskar (Privat) on 2013-07-08.
//  Copyright (c) 2013 Oskar Wickström. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ScoreSpecification.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSImageView *scoreImageView;
@property (assign) IBOutlet NSProgressIndicator *progressIndicator;

@property (strong) NSArray *keySignatures;
@property (strong) NSArray *modes;
@property (strong) NSArray *difficulties;

@property (strong) ScoreSpecification *scoreSpecification;
@property (strong) NSImage *scoreImage;

@property (strong) NSNumber *isRefreshing;
@property (strong) NSNumber *expectedResponseSize;
@property (strong) NSNumber *progress;
@property NSMutableData *responseData;

@end

