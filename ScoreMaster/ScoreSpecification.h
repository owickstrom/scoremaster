//
//  ScoreSpecification.h
//  ScoreMaster
//
//  Created by Oskar (Privat) on 2013-07-08.
//  Copyright (c) 2013 Oskar Wickström. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScoreSpecification : NSObject

@property (strong) NSString *keySignature;
@property (strong) NSString *mode;
@property (strong) NSNumber *difficulty;

- (id)initWithKeySignature:(NSString*)keySignature AndMode:(NSString*)mode AndDifficulty:(NSNumber*)difficulty;

-(NSString*) difficultyAsString;

@end
